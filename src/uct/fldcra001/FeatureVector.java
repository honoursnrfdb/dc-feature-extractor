package uct.fldcra001;

import java.io.*;
        import java.text.DateFormatSymbols;
        import java.util.*;

/**
 * Created by Craig Feldman on 10-09-15.
 *
 * Used to store and calculate features of a string of data
 */
public class FeatureVector {
    private int length;
    private int numWords;
    private int numMonthNames;
    private int numPersonNames;
    private int numDigits;
    private int numLetters;
    private int numNotAlphaNumeric; // e.g. .>/" etc
    private int numCapitals;


    // Month names
    private Set<String> monthSet;
    // Stores names from a dictionary of names
    private Set<String> nameSet;
    // Change name database here if required
    private final String NAMES_FILENAME = "names.txt";

    // Stores the features to be returned
    List<String> features;


    public FeatureVector(int numFeatures){
        features = new LinkedList<>();
        resetValues();

        // Change here if you want localised month names
        String[] longMonthNames = new DateFormatSymbols().getMonths();
        String[] shortMonthNames = new DateFormatSymbols().getShortMonths();
        monthSet = new HashSet<>();
        for (String monthName : longMonthNames)
            monthSet.add(monthName.toLowerCase());
        for (String monthName : shortMonthNames)
            monthSet.add(monthName.toLowerCase());


        nameSet = new HashSet<String>();
        try (BufferedReader br = new BufferedReader(new FileReader(NAMES_FILENAME))) {
            String line;
            while ((line = br.readLine()) != null) {
                nameSet.add(line);
            }
        } catch (IOException e) {
            System.err.println(e.toString());
            System.err.println("Error accessing names database '" + NAMES_FILENAME + "'");
            System.err.println("Program will continue without names database. This is not recommended.");
            // e.printStackTrace();
        }
    }

    private void resetValues() {
        length = 0;
        numDigits = 0;
        numLetters = 0;
        numNotAlphaNumeric = 0;
        numCapitals = 0;
        numWords = 0;
        numMonthNames = 0;
        numPersonNames = 0;

        features.clear();
    }


    public void generateFeatures(String data, String fieldName) {
        resetValues();
        length = data.length();

        for (int i = 0; i < length; ++i) {
            char c = data.charAt(i);

            if (Character.isDigit(c))
                ++numDigits;

            else if (Character.isLetter(c)) {
                ++numLetters;
                // count capitals
                if (Character.isUpperCase(c))
                    ++numCapitals;
            }
            else if (!Character.isWhitespace(c)) {// not letter or digit e.g. ./?";\
                ++numNotAlphaNumeric;
            }
        }


        String[] parts = data.split(" ");

        // Get the number of parts
        numWords = parts.length;

        // Perfomance heuristic to not check for month names on long strings
        //TODO verify if this should be done
        if (true) {
            for (String part : parts) {
                // Regex used to remove non unicode characters (e.g. 14/Sep/2015 -> Sep -> sep)
                String alphaPart = part.replaceAll("\\P{L}+", "").toLowerCase();
                // Need to also prevent a blank string from matching
                if (alphaPart.length() == 0)
                    continue;

                if (monthSet.contains(alphaPart))
                    ++numMonthNames;

                    // People may have a name like 'August', but it is more likely that that is a month name and hence 'else if'
                else if (nameSet.contains(alphaPart))
                    ++numPersonNames;
            }
        }

        generateFinalFeatures(fieldName);

    }

    private void generateFinalFeatures(String fieldName) {
        double dLength = (double) length;
        //features.add(fieldName);
        features.add(length + "");
        features.add(numWords +"");
        features.add(numMonthNames + "");
        features.add(numPersonNames + "");
        features.add(numDigits / dLength + "");
        features.add(numLetters / dLength + "");
        features.add(numNotAlphaNumeric / dLength + "");
        features.add(numCapitals / dLength + "");

    }

    @Override
    public String toString() {
        return "FeatureVector{" +
                "length=" + length +
                ", numWords=" + numWords +
                ", numMonthNames=" + numMonthNames +
                ", numPersonNames=" + numPersonNames +
                ", numDigits=" + numDigits +
                ", numLetters=" + numLetters +
                ", numNotAlphaNumeric=" + numNotAlphaNumeric +
                ", numCapitals=" + numCapitals +
                '}';
    }


    public List<String> getFeatures() {
        //List<Double> features = new LinkedList<Double>();
        return features;
    }
}
