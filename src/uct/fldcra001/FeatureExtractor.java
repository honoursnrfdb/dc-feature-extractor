package uct.fldcra001;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Craig Feldman on 10-09-15.
 *
 * Extracts features from a text file
 */
public class FeatureExtractor {

    // stores the attributes that are used for ML
    private static final Map<String, String> attributes;
    // Class names (DC fields) - uses a set so we can do some quick error handling.
    private static final String[] FIELD_VALUES = new String[] {
            "contributor",
            "coverage",
            "creator",
            "date",
            "description",
            "format",
            "identifier",
            "language",
            "publisher",
            "relation",
            "rights",
            "source",
            "subject",
            "title",
            "type"
    };
    // Single FV object that will be reused
    static FeatureVector fv;

    static {
        attributes = new LinkedHashMap<>();
        attributes.put("number of characters", "numeric");
        attributes.put("number of words", "numeric");
        attributes.put("number of months", "numeric");
        attributes.put("number of person names", "numeric");
        attributes.put("% of digits", "numeric");
        attributes.put("% of letters", "numeric");
        attributes.put("% of non-alphanumeric", "numeric");
        attributes.put("% of capitals", "numeric");
    }

    // FileInputStream inputFile;
    BufferedWriter writer;
    Set<String> DC_FIELDS = new HashSet<>(Arrays.asList(FIELD_VALUES));

    private String outputFileName;

    public FeatureExtractor(String outputFileName) {
        // Create a single FV object, that will be reused
        fv = new FeatureVector(attributes.size());
        this.outputFileName = outputFileName;

        createOutputFile();
    }

    // Create the output arff file
    private void createOutputFile() {
        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(outputFileName), "utf-8"));

            writer.write("% This file is used by Weka to train an algorithm to learn how to map elements to the dublin core metadata fields");
            writer.newLine();
            writer.newLine();

            // name of the relation
            writer.write("@relation dcmapper");
            writer.newLine();

            writeAttributes();

            writer.newLine();
            writer.write("@data");
            writer.newLine();

            // Flush to file
            writer.flush();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Writes attributes to output file.
     * example output:
     *      \@attribute "% of person names" numeric
     *      \@attribute class {contributor, coverage, creator, date}
     * @throws IOException
     */
    private void writeAttributes() throws IOException {
        writer.newLine();
        for(String s: attributes.keySet()) {
            writer.write("@attribute");
            writer.write(" \"" + s + "\" ");
            writer.write(attributes.get(s));
            writer.newLine();
        }

        // write the class names
        writer.write("@attribute class {");
        int numClasses = FIELD_VALUES.length;
        for (int i = 0; i < numClasses - 1; ++i)
            writer.write(FIELD_VALUES[i]+ ", ");
        writer.write(FIELD_VALUES[numClasses - 1] + "}");
        writer.newLine();
    }

    /**
     * Process all the file in a given directory
     * @param directory the directory to transverse
     * @throws IOException
     */
    public void processFiles (String directory) throws IOException {
        List<File> filesInFolder = Files.walk(Paths.get(directory))
                .filter(Files::isRegularFile)
                .map(Path::toFile)
                .collect(Collectors.toList());

        for (File f : filesInFolder)
            processFile(f);

        writer.close();
    }

    /**
     * Process an individual input file
     * @param f the file to process
     */
    public void processFile(File f) {
        String line = null;

        int lineNumber = 0;

         try(BufferedReader in = new BufferedReader(new FileReader(f))){
            writer.write("% --- " + f.getName() + " --- ");
            writer.newLine();

            while((line = in.readLine()) != null) {
                ++lineNumber;
                processLine(line, lineNumber, f.getName());
            }
        } catch (FileNotFoundException e) {
            System.err.println("Input file not found. Exiting.");
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
             System.err.println("Error processing file: " + f.getName() + " | @ Line number " + lineNumber);
             if (line != null)
                 System.err.println("The offending line is: " + line.toString());

         }
    }

    /** Processes an individual line and extracts class and data
     *
     * @param line the line to process
     * @param lineNumber the location of the line in the file.
     * @param fileName the name of the file being processed     *
     * @throws IOException
     */
    private void processLine(String line, int lineNumber, String fileName) throws IOException {

        // Split line into data and field name
        int posFieldStart = line.indexOf("<dc:");
        int posFieldEnd = line.indexOf('>');
        int posLineEnd = line.lastIndexOf("</");

        if (posFieldStart == -1 || posFieldEnd == -1) {
            System.err.printf("Error processing line %d of file %s.\n\tInvalid syntax'.\n\t", lineNumber, fileName);
            System.err.println("Skipping line and continuing.");
            return;
        }

        String fieldName = line.substring(posFieldStart + 4, posFieldEnd);
        String data = line.substring(posFieldEnd + 1, posLineEnd).trim();

        if (!DC_FIELDS.contains(fieldName)) {
            System.err.printf("Error processing line %d of file %s.\n\tInvalid class name '%s'.\n\t", lineNumber, fileName, fieldName);
            System.err.println("Skipping line and continuing.");
            return;
        }

        fv.generateFeatures(data, fieldName);

        /*
        System.out.println(fieldName + ": " + data);
        System.out.println(fv.toString());
        System.out.println(fv.getFeatures().toString());
        System.out.println();
        */

        writeData(fieldName, fv.getFeatures());
    }

    /**
     * Writes data to arff file in the form "x, y, z, class"
     * i.e. a series of values and the class name.
     * @param fieldName
     * @param features
     * @throws IOException
     */
    private void writeData(String fieldName, List<String> features) throws IOException {
        int size = features.size();
        if (size != attributes.size()) {
            System.err.println("Critical error. Attribute numbers do not match. Aborting.");
            System.err.printf("Found %d features. Expected %d.", size, attributes.size());
            System.exit(1);
        }
        for(String feature : features) {
            writer.write(feature + ", ");
        }

        writer.write(fieldName);

        writer.newLine();
        writer.flush();

    }

}
