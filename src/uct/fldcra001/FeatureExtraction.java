package uct.fldcra001;

import java.io.IOException;

public class FeatureExtraction {

    /*
     (?s)<dc:(.*?>)(.*?)\1
     (?<=[^>])( )?\n(?=[^<])
     ( )?\n(?=</)|\n(?=[^<])
     */

    static String outputFileName;

    private final static String OUTPUT_FILENAME = "output.arff";
    private final static String INPUT_DIRECTORY = "input_data";

    public static void main(String[] args) {
        System.out.println("Extracting features from " + INPUT_DIRECTORY);
        System.out.println("Creating arff file " + OUTPUT_FILENAME);


        FeatureExtractor fe = new FeatureExtractor(OUTPUT_FILENAME);
        // Extracts the features from the input files
        try {
            fe.processFiles(INPUT_DIRECTORY);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
